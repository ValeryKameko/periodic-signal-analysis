# periodic-signal-analysis

Periodic signal analysis on GPU with C++ and OpenCL

![Application UI view](./res/screenshot1.png)

## What is this?

It's application created to analyse errors of signal parameters based on different configuration.
More specifically signal is:

<pre><code>x(n) = sin (2 * &pi; * n / N + &phi;)
</code></pre>

And statistics are the following for each M from `K .. 2N`:
* Amplitude, calculated as: <code>A = 1 - |(&sum; x(n) * e<sup>2&pi;in / M </sup>)| / M</code>
* Mean square value, calculated as: <code>X<sub>msv</sub> = sqrt(&sum; x(n)<sup>2</sup> / M)</code>
* Mean square value v2, calculated as: <code>X<sub>msv</sub> = sqrt(&sum; x(n)<sup>2</sup> / M - (&sum; x(n) / M)<sup>2</sup>)</code>

Created with:
* [SDL2](https://www.libsdl.org/)
* [ImGui](https://github.com/ocornut/imgui/) with [ImPlot](https://github.com/epezent/implot)
* [OpenCL](https://wikipedia.org/wiki/OpenCL) v1.2

## How to build

Requirements:
* OpenGL >= 4.2, or otherwise:
    * change **_kOpenGlVersion_** and **_kGlslVersion_** in [application_configuration.h](./app/application_configuration.h))
    * change **_API_** version in [glad2_loader/CMakeLists.txt](./3rd_party/glad2_loader/CMakeLists.txt)
* C++ compiler supporting C++20 standard
* CMake >= 3.15
    * otherwise, try change **_VERSION_** in [CMakeLists.txt](./CMakeLists.txt)
* Installed [SDL2](https://www.libsdl.org/download-2.0.php) library
* Installed OpenCL environment:
    * for AMD &mdash; [AMD Accelerated Parallel Processing SDK](https://en.wikipedia.org/wiki/AMD_APP_SDK)
    * for Nvidia &mdash; [CUDA toolkit](https://developer.nvidia.com/CUDA-toolkit-Archive)
    * for Intel &mdash; [Intel SDK for OpenCL](https://software.intel.com/content/www/us/en/develop/tools/opencl-sdk.html)
    

---
**Warning!:** Project dependencies is almost **100 MB**

---

Clone project with submodules (choose one of the repositories):
```shell script
git clone https://github.com/ValeryKameko/periodic-signal-analysis --recurse-submodules
git clone https://gitlab.com/ValeryKameko/periodic-signal-analysis --recurse-submodules
```
Build CMake project:
```shell script
cd    ./periodic-signal-analysis
mkdir ./cmake-build-release
cd    ./cmake-build-release
cmake .. -DCMAKE_BUILD_TYPE=Release
```

To run application:
```shell script
cd ./bin

# for WINDOWS
./periodic-signal-analysis.exe

# for Linux
./periodic-signal-analysis
```

Tested on:
* Windows (GNU GCC 9.0 MinGW, CMake 3.15, OpenGL 4.6)
    + Nvidia CUDA 11.2 + OpenCL 1.2
    + Intel SDK + OpenCL 2.1
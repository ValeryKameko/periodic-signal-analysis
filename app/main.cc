#include <periodic_signal_analyzer_model.h>
#include <periodic_signal_analyzer_view.h>

#include <memory>

#include "application.h"

namespace view = periodic_signal_analyzer::view;
namespace model = periodic_signal_analyzer::model;

auto main(int argc, char **argv) -> int {
    periodic_signal_analyzer::Application application{argc, argv};

    auto return_code = application.Init();
    if (return_code != 0) {
        return return_code;
    }

    model::PeriodicSignalAnalyzerModel model{};
    auto periodic_signal_analyzer = std::make_unique<view::PeriodicSignalAnalyzerView>(model);
    application.AddView(std::move(periodic_signal_analyzer));

    return_code = application.Run();
    return return_code;
}

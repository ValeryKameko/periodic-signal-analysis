#ifndef PERIODIC_SIGNAL_ANALYZER_APPLICATION_CONFIGURATION_H
#define PERIODIC_SIGNAL_ANALYZER_APPLICATION_CONFIGURATION_H

#include <IconsForkAwesome.h>

#include <string>

namespace periodic_signal_analyzer::configuration {

enum class ApplicationTheme { kClassic, kLight, kDark };

struct OpenGlVersion {
    int major;
    int minor;
};

constexpr auto kWindowWidth = 1280;
constexpr auto kWindowHeight = 720;

constexpr auto kVsyncValue = -1;  // Adaptive VSYNC
constexpr auto kImGuiTheme = ApplicationTheme::kClassic;

constexpr auto kOpenGlVersion = OpenGlVersion{.major = 4, .minor = 2};
constexpr auto kGlslVersion = "#version 420";

const std::string kResourcePath = "./res/";
const std::string kRobotoFontPath = kResourcePath + "fonts/Roboto-Medium.ttf";
const std::string kForkAwesomeFontPath = kResourcePath + "fonts/" + FONT_ICON_FILE_NAME_FK;

}  // namespace periodic_signal_analyzer::configuration

#endif  // PERIODIC_SIGNAL_ANALYZER_APPLICATION_CONFIGURATION_H

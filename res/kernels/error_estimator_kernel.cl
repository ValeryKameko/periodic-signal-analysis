typedef struct __attribute__ ((packed)) Parameters {
    float phi;
    uint k;
    uint n;
} Parameters;

typedef struct __attribute__ ((packed)) PointResult {
    uint m;
    float value;
    float mean_square_value_error[2];
    float amplitude_error;
} PointResult;

void error_estimate(uint m, __constant const struct Parameters* parameters,
                    __global struct PointResult* point_result) {
    float sum_square_value = 0;
    float sum_value = 0;
    float2 sum_sin_cos = 0;

    for (uint i = 0; i < m; i++) {
        float value = sin(2 * M_PI_F * i / parameters->n + parameters->phi);

        sum_square_value += pown(value, 2);
        sum_value += value;

        float angle = 2 * M_PI_F * i / m;
        sum_sin_cos += value * (float2)(sin(angle), cos(angle));
    }
    sum_sin_cos /= m;

    point_result->m = m;
    point_result->value = sin(2 * M_PI_F * m / parameters->n + parameters->phi);
    point_result->mean_square_value_error[0] = M_SQRT1_2_F - sqrt(sum_square_value / (m + 1));
    point_result->mean_square_value_error[1] = M_SQRT1_2_F - sqrt(sum_square_value / (m + 1) - pown(sum_value / (m + 1), 2));
    point_result->amplitude_error = 1 - sqrt(pown(sum_sin_cos.s0, 2) + pown(sum_sin_cos.s1, 2));
}

__kernel void error_estimator_kernel(__global struct PointResult* point_results,
                                     __constant const struct Parameters* parameters) {
    for (uint m = parameters->k + get_global_id(0); m <= 2 * parameters->n;
         m += get_global_size(0)) {
        uint i = m - parameters->k;
        error_estimate(m, parameters, &point_results[i]);
    }
}
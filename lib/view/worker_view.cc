#include "worker_view.h"

#include <imgui.h>

#include <cmath>
#include <numbers>
#include <utility>

namespace periodic_signal_analyzer::view {

struct Option {
    std::u8string view;
    float value;
};

static std::vector<Option> kKOptions{
    Option{.view = u8"0", .value = 0},
    Option{.view = u8"N / 4", .value = 1.0 / 4},
    Option{.view = u8"3 * N / 4", .value = 3.0 / 4},
};

static std::vector<Option> kPhiOptions{
    Option{.view = u8"PI / 32", .value = 1.0 / 32},
    Option{.view = u8"PI / 16", .value = 1.0 / 16},
    Option{.view = u8"PI / 8", .value = 1.0 / 8},
    Option{.view = u8"PI / 4", .value = 1.0 / 4},
    Option{.view = u8"PI / 2", .value = 1.0 / 2},
    Option{.view = u8"2 * PI / 3", .value = 2.0 / 3},
};

static unsigned int kNOptionStartIndex = 6;
static unsigned int kNOptionsCount = 15;

WorkerView::WorkerView(WorkerView::Model& model, std::string window_name)
    : model_{model}, window_name_{std::move(window_name)} {}

auto WorkerView::GetWindowName() -> const std::string& { return window_name_; }

void WorkerView::Render() {
    ImGuiWindowFlags window_flags{ImGuiWindowFlags_HorizontalScrollbar |
                                  ImGuiWindowFlags_NoScrollWithMouse};
    if (ImGui::Begin(window_name_.c_str(), nullptr, window_flags)) {
        const std::string header = "Parameters";
        const float font_size = ImGui::GetFontSize() * header.size() / 2;
        ImGui::SameLine(ImGui::GetWindowSize().x / 2 - (font_size / 2));
        ImGui::Text("%s", header.c_str());
        ImGui::Separator();

        std::string n_preview = std::to_string(GetNValue(n_option_index_));
        if (ImGui::BeginCombo("N", n_preview.c_str())) {
            for (unsigned int i = 0; i < kNOptionsCount; i++) {
                const auto& option = GetNValue(i);
                bool is_selected = i == n_option_index_;
                if (ImGui::Selectable(std::to_string(option).c_str(), &is_selected))
                    n_option_index_ = i;
                if (is_selected) ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        std::u8string k_preview = kKOptions[k_option_index_].view;
        if (ImGui::BeginCombo("K", reinterpret_cast<const char*>(k_preview.c_str()))) {
            for (unsigned int i = 0; i < kKOptions.size(); i++) {
                const auto& option = kKOptions[i];
                bool is_selected = i == k_option_index_;
                if (ImGui::Selectable(reinterpret_cast<const char*>(option.view.c_str()),
                                      &is_selected))
                    k_option_index_ = i;
                if (is_selected) ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        std::u8string phi_preview = kPhiOptions[phi_option_index_].view;
        if (ImGui::BeginCombo("Phi", reinterpret_cast<const char*>(phi_preview.c_str()))) {
            for (unsigned int i = 0; i < kPhiOptions.size(); i++) {
                const auto& option = kPhiOptions[i];
                bool is_selected = i == phi_option_index_;
                if (ImGui::Selectable(reinterpret_cast<const char*>(option.view.c_str()),
                                      &is_selected))
                    phi_option_index_ = i;
                if (is_selected) ImGui::SetItemDefaultFocus();
            }
            ImGui::EndCombo();
        }

        if (ImGui::Button("Run", ImVec2{120, 0})) {
            unsigned int n = GetNValue(n_option_index_);
            model_.SetWorkParameters(model::Worker::Parameters{
                .n = n,
                .k = static_cast<unsigned int>(n * kKOptions[k_option_index_].value),
                .phi = static_cast<float>(std::numbers::pi * kPhiOptions[phi_option_index_].value),
            });
            model_.RunWorker();
        }

        ImGui::End();
    }
}

auto WorkerView::GetNValue(unsigned int index) -> unsigned int {
    return 1 << (index + kNOptionStartIndex);
}

}  // namespace periodic_signal_analyzer::view

#ifndef PERIODIC_SIGNAL_ANALYZER_WORKER_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_WORKER_VIEW_H

#include <periodic_signal_analyzer_model.h>

#include "view.h"

namespace periodic_signal_analyzer::view {

class WorkerView final : public View {
  public:
    using Model = model::PeriodicSignalAnalyzerModel;

    explicit WorkerView(Model& model, std::string window_name = "worker_view");

    auto GetWindowName() -> const std::string&;

    void Render() override;

  private:
    auto GetNValue(unsigned int index) -> unsigned int;

    Model& model_;
    std::string window_name_;

    unsigned int n_option_index_{0};
    unsigned int k_option_index_{0};
    unsigned int phi_option_index_{0};
};

}  // namespace periodic_signal_analyzer::view

#endif  // PERIODIC_SIGNAL_ANALYZER_WORKER_VIEW_H

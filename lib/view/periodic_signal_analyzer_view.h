#ifndef PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_VIEW_H

#include <periodic_signal_analyzer_model.h>
#include <worker_picker_view.h>

#include "graph_view.h"
#include "open_cl_info_view.h"
#include "view.h"
#include "worker_view.h"

namespace periodic_signal_analyzer::view {

class PeriodicSignalAnalyzerView final : public View {
  public:
    using Model = model::PeriodicSignalAnalyzerModel;

    explicit PeriodicSignalAnalyzerView(Model& model);
    void Render() override;

  private:
    bool BeginDockingWindow();
    void EndDockingWindow();
    void InitDockingLayout();
    void RenderMenuBar();
    void RenderErrorPopup();
    void RenderWorkerPopup();

    Model& model_;
    OpenClInfoView opencl_info_view_;
    WorkerPickerView worker_picker_view_;
    WorkerView worker_view_;
    GraphView graph_view_;
};

}  // namespace periodic_signal_analyzer::view

#endif  // PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_VIEW_H

#include "graph_view.h"

#include <imgui.h>
#include <implot.h>

#include <utility>

namespace periodic_signal_analyzer::view {

GraphView::GraphView(Model& model, std::string window_name)
    : model_{model}, window_name_{std::move(window_name)} {}

auto GraphView::GetWindowName() -> const std::string& { return window_name_; }

void GraphView::Render() {
    if (ImGui::Begin(window_name_.c_str())) {
        auto& result = model_.GetWorkResult();

        if (cached_parameters_ != result.parameters) {
            cached_parameters_ = result.parameters;
            ImPlot::SetNextPlotLimits(result.parameters.k, 2 * result.parameters.n, -1, 1.5,
                                      ImGuiCond_Always);
        }
        if (ImPlot::BeginPlot("Graph", nullptr, nullptr, ImVec2{-1, -1})) {
            ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle, 2.0);
            ImPlot::PlotLineG(
                "Mean square value error 1",
                [](void* data, int idx) -> ImPlotPoint {
                    const auto point_result = static_cast<model::Worker::PointResult*>(data) + idx;
                    return {static_cast<double>(point_result->m),
                            point_result->mean_square_value_error[0]};
                },
                reinterpret_cast<void*>(result.point_results.data()), result.point_results.size());

            ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle, 2.0);
            ImPlot::PlotLineG(
                "Mean square value error 2",
                [](void* data, int idx) -> ImPlotPoint {
                    const auto point_result = static_cast<model::Worker::PointResult*>(data) + idx;
                    return {static_cast<double>(point_result->m),
                            point_result->mean_square_value_error[1]};
                },
                reinterpret_cast<void*>(result.point_results.data()), result.point_results.size());

            ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle, 2.0);
            ImPlot::PlotLineG(
                "Amplitude error",
                [](void* data, int idx) -> ImPlotPoint {
                    const auto point_result = static_cast<model::Worker::PointResult*>(data) + idx;
                    return {static_cast<double>(point_result->m), point_result->amplitude_error};
                },
                reinterpret_cast<void*>(result.point_results.data()), result.point_results.size());
            ImPlot::EndPlot();
        }

        ImGui::End();
    }
}

}  // namespace periodic_signal_analyzer::view
#ifndef PERIODIC_SIGNAL_ANALYZER_WORKER_PICKER_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_WORKER_PICKER_VIEW_H

#include <periodic_signal_analyzer_model.h>

#include "view.h"

namespace periodic_signal_analyzer::view {

class WorkerPickerView : public view::View {
  public:
    using Model = model::PeriodicSignalAnalyzerModel;

    explicit WorkerPickerView(Model& model);

    void Show();
    void Render() override;

  private:
    void RenderPlatformCombo(const model::OpenClInfo& opencl_info);
    void RenderDeviceCombo(const model::OpenClInfo& opencl_info);

    Model& model_;

    bool show_{};
    std::optional<cl::Platform> current_platform_{};
    std::optional<cl::Device> current_device_{};
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_WORKER_PICKER_VIEW_H

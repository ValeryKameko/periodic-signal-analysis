#ifndef PERIODIC_SIGNAL_ANALYZER_GRAPH_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_GRAPH_VIEW_H

#include <periodic_signal_analyzer_model.h>

#include <string>

#include "view.h"
#include "worker_view.h"

namespace periodic_signal_analyzer::view {

class GraphView final : public View {
  public:
    using Model = model::PeriodicSignalAnalyzerModel;

    explicit GraphView(Model& model, std::string window_name = "graph_view");

    auto GetWindowName() -> const std::string&;

    void Render() override;

  private:
    Model& model_;
    std::string window_name_;
    model::Worker::Parameters cached_parameters_{};
};

}  // namespace periodic_signal_analyzer::view

#endif  // PERIODIC_SIGNAL_ANALYZER_GRAPH_VIEW_H

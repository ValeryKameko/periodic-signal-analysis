#include "periodic_signal_analyzer_view.h"

#include <imgui.h>
#include <imgui_internal.h>

#include <string>

namespace periodic_signal_analyzer::view {

const std::string kDockspaceName = "main_dockspace";

PeriodicSignalAnalyzerView::PeriodicSignalAnalyzerView(Model& model)
    : model_{model},
      opencl_info_view_{model},
      worker_picker_view_{model},
      worker_view_{model},
      graph_view_{model} {}

void PeriodicSignalAnalyzerView::Render() {
    if (BeginDockingWindow()) {
        RenderMenuBar();

        ImGuiWindowClass window_class;
        window_class.DockNodeFlagsOverrideSet = ImGuiDockNodeFlags_NoTabBar;

        ImGui::SetNextWindowClass(&window_class);
        worker_view_.Render();

        ImGui::SetNextWindowClass(&window_class);
        graph_view_.Render();

        EndDockingWindow();
    }

    opencl_info_view_.Render();
    worker_picker_view_.Render();
    RenderErrorPopup();
}

auto PeriodicSignalAnalyzerView::BeginDockingWindow() -> bool {
    ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->GetWorkPos());
    ImGui::SetNextWindowSize(viewport->GetWorkSize());

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse |
                    ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove |
                    ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus |
                    ImGuiWindowFlags_NoBackground;

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0F);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2{});
    bool is_open = ImGui::Begin("DockSpace", nullptr, window_flags);
    ImGui::PopStyleVar(2);

    if (is_open) {
        InitDockingLayout();
        ImGui::DockSpace(ImGui::GetID(kDockspaceName.c_str()), {0, 0});
        return true;
    }
    return false;
}

void PeriodicSignalAnalyzerView::InitDockingLayout() {
    const ImGuiID dockspace_id = ImGui::GetID(kDockspaceName.c_str());
    if (ImGui::DockBuilderGetNode(dockspace_id) == nullptr) {
        ImGui::DockBuilderRemoveNode(dockspace_id);
        ImGui::DockBuilderAddNode(dockspace_id, ImGuiDockNodeFlags_DockSpace);
        ImGui::DockBuilderSetNodeSize(dockspace_id, ImGui::GetWindowViewport()->Size);

        ImGuiID dock_main_id = dockspace_id;
        ImGuiID dock_left_id =
            ImGui::DockBuilderSplitNode(dock_main_id, ImGuiDir_Left, 0.4, nullptr, &dock_main_id);

        ImGui::DockBuilderDockWindow(worker_view_.GetWindowName().c_str(), dock_left_id);
        ImGui::DockBuilderDockWindow(graph_view_.GetWindowName().c_str(), dock_main_id);

        ImGui::DockBuilderFinish(dockspace_id);
    }
}

void PeriodicSignalAnalyzerView::EndDockingWindow() { ImGui::End(); }

void PeriodicSignalAnalyzerView::RenderMenuBar() {
    if (ImGui::BeginMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            if (ImGui::MenuItem("Exit")) {
                SetWantClose();
            }
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Worker")) {
            if (ImGui::MenuItem("Choose worker")) {
                worker_picker_view_.Show();
            }
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Info")) {
            if (ImGui::MenuItem("OpenCL")) {
                opencl_info_view_.Show();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }
}

void PeriodicSignalAnalyzerView::RenderErrorPopup() {
    if (auto error = model_.GetCurrentError()) {
        auto title = std::string(error->what_title()) + "#error";
        ImGui::OpenPopup(title.c_str());

        if (ImGui::BeginPopupModal(title.c_str())) {
            ImGui::Text("%s", error->what());
            ImGui::EndPopup();
        } else {
            model_.SetCurrentError(std::nullopt);
        }
    }
}

}  // namespace periodic_signal_analyzer::view

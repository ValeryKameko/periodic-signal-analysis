#ifndef PERIODIC_SIGNAL_ANALYZER_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_VIEW_H

namespace periodic_signal_analyzer::view {

class View {
  public:
    View() = default;
    virtual ~View() = default;

    virtual void Render() = 0;
    virtual auto WantClose() -> bool;

  protected:
    void SetWantClose();

  private:
    bool want_close_{false};
};

}  // namespace periodic_signal_analyzer::view

#endif  // PERIODIC_SIGNAL_ANALYZER_VIEW_H

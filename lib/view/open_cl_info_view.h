#ifndef PERIODIC_SIGNAL_ANALYZER_OPEN_CL_INFO_VIEW_H
#define PERIODIC_SIGNAL_ANALYZER_OPEN_CL_INFO_VIEW_H

#include <error.h>
#include <periodic_signal_analyzer_model.h>

#include <optional>

#include "view.h"

namespace periodic_signal_analyzer::view {

class OpenClInfoView final : public view::View {
  public:
    using Model = model::PeriodicSignalAnalyzerModel;

    explicit OpenClInfoView(Model& model);

    void Show();
    void Render() override;

  private:
    void RenderPlatform(const model::OpenClPlatformInfo& platform_info);
    void RenderDevice(const model::OpenClDeviceInfo& device_info);
    auto FormatByteSize(unsigned long long size) -> std::string;

    Model& model_;
    std::optional<model::OpenClInfo> opencl_info_{};
    bool show_{};
};

}  // namespace periodic_signal_analyzer::view

#endif  // PERIODIC_SIGNAL_ANALYZER_OPEN_CL_INFO_VIEW_H

#include "view.h"

namespace periodic_signal_analyzer::view {

auto View::WantClose() -> bool { return want_close_; }

void View::SetWantClose() { want_close_ = true; }

}  // namespace periodic_signal_analyzer::view

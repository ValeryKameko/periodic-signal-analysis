#ifndef PERIODIC_SIGNAL_ANALYZER_OPENCL_WORKER_H
#define PERIODIC_SIGNAL_ANALYZER_OPENCL_WORKER_H

#include "opencl.h"
#include "worker.h"

namespace periodic_signal_analyzer::model {

class OpenClWorker : public Worker {
  public:
    OpenClWorker(const cl::Context& context);

    auto DoWork(const Parameters& parameters) -> Result override;

  private:
    auto BuildProgram(const cl::Context& context, cl::Program::Sources program_sources)
        -> cl::Program;

    const cl::Context& context_;
    cl::Kernel kernel_;
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_OPENCL_WORKER_H

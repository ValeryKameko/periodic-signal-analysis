#include "cpu_worker.h"

#include <cmath>
#include <numbers>
#include <vector>

namespace periodic_signal_analyzer::model {

auto CpuWorker::DoWork(const Parameters& parameters) -> Result {
    size_t point_result_count = 2 * parameters.n - parameters.k + 1;
    std::vector<PointResult> point_results(point_result_count);

    auto point_results_it = std::begin(point_results);
    for (unsigned int m = parameters.k; m <= 2 * parameters.n; m++, point_results_it++) {
        float sum_square_value = 0;
        float sum_value = 0;
        float sum_sin = 0;
        float sum_cos = 0;

        for (unsigned int i = 0; i < m; i++) {
            float value = std::sin(2 * std::numbers::pi * i / parameters.n + parameters.phi);

            sum_square_value += std::pow(value, 2);
            sum_value += value;

            float angle = 2 * std::numbers::pi * i / m;
            sum_sin += value * std::sin(angle);
            sum_cos += value * std::cos(angle);
        }

        sum_sin /= m;
        sum_cos /= m;

        float value = std::sin(2 * std::numbers::pi * m / parameters.n + parameters.phi);
        float mean_square_value_error0 =
            1 / std::numbers::sqrt2 - std::sqrt(sum_square_value / (m + 1));
        float mean_square_value_error1 =
            1 / std::numbers::sqrt2 -
            std::sqrt(sum_square_value / (m + 1) - std::pow(sum_value / (m + 1), 2));
        float amplitude_error = 1.0 - std::sqrt(std::pow(sum_sin, 2) + std::pow(sum_cos, 2));

        *point_results_it = PointResult{
            .m = m,
            .value = value,
            .mean_square_value_error = {mean_square_value_error0, mean_square_value_error1},
            .amplitude_error = amplitude_error,
        };
    }

    return Result{
        .parameters = parameters,
        .point_results = point_results,
    };
}

}  // namespace periodic_signal_analyzer::model

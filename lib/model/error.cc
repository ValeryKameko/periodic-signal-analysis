#include "error.h"

namespace periodic_signal_analyzer::model {

Error::Error(const std::string& what_arg, const std::string& what_title)
    : std::runtime_error{what_arg}, what_title_{std::in_place, what_title} {}

Error::Error(const char* what_arg, const char* what_title)
    : std::runtime_error{what_arg}, what_title_{std::in_place, what_title} {}

const char* Error::what_title() const { return what_title_.value_or("Runtime error").c_str(); }

}  // namespace periodic_signal_analyzer::model

#ifndef PERIODIC_SIGNAL_ANALYZER_ERROR_H
#define PERIODIC_SIGNAL_ANALYZER_ERROR_H

#include <optional>
#include <stdexcept>

namespace periodic_signal_analyzer::model {

class Error : public std::runtime_error {
  public:
    explicit Error(const std::string& what_arg, const std::string& what_title = "Error");
    explicit Error(const char* what_arg, const char* what_title = "Error");
    Error(const Error& other) noexcept = default;

    const char* what_title() const;

  private:
    std::optional<std::string> what_title_{};
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_ERROR_H

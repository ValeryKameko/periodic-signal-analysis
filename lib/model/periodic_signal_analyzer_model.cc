#include "periodic_signal_analyzer_model.h"

#include "cpu_worker.h"
#include "error.h"
#include "opencl.h"
#include "opencl_worker.h"

namespace periodic_signal_analyzer::model {

PeriodicSignalAnalyzerModel::PeriodicSignalAnalyzerModel() {
    opencl_info_ = GatherOpenClInfo();
    InitWorker();
}

auto PeriodicSignalAnalyzerModel::GatherOpenClInfo() -> OpenClInfo {
    OpenClInfo info{};
    std::vector<OpenClPlatformInfo> platform_infos;
    std::vector<cl::Platform> platforms;

    try {
        cl::Platform::get(&platforms);
        for (auto& platform : platforms) {
            OpenClPlatformInfo platform_info;
            platform_info.platform = platform;

            std::vector<cl::Device> devices;
            platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
            for (auto& device : devices) {
                platform_info.devices.push_back(OpenClDeviceInfo{.device = device});
            }

            info.platforms.push_back(std::move(platform_info));
        }
        return info;
    } catch (cl::Error& error) {
        throw Error{error.what()};
    }
}
void PeriodicSignalAnalyzerModel::SetCurrentError(std::optional<Error> error) { error_ = error; }

auto PeriodicSignalAnalyzerModel::GetCurrentError() -> const std::optional<Error>& {
    return error_;
}

void PeriodicSignalAnalyzerModel::InitWorker(std::optional<cl::Device> device) {
    device_ = device;

    if (device) {
        context_ = cl::Context{*device};
        worker_ = std::make_unique<OpenClWorker>(*context_);
    } else {
        context_ = std::nullopt;
        worker_ = std::make_unique<CpuWorker>();
    }
}

auto PeriodicSignalAnalyzerModel::GetOpenClInfo() -> const OpenClInfo& { return opencl_info_; }

auto PeriodicSignalAnalyzerModel::GetCurrentDevice() -> const std::optional<cl::Device>& {
    return device_;
}

auto PeriodicSignalAnalyzerModel::GetWorkParameters() -> const Worker::Parameters& {
    return work_parameters_;
}

void PeriodicSignalAnalyzerModel::SetWorkParameters(const Worker::Parameters& parameters) {
    work_parameters_ = parameters;
}

auto PeriodicSignalAnalyzerModel::GetWorkResult() -> Worker::Result& { return work_result_; }

void PeriodicSignalAnalyzerModel::RunWorker() { work_result_ = worker_->DoWork(work_parameters_); }

}  // namespace periodic_signal_analyzer::model

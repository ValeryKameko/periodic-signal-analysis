#include "opencl_worker.h"

#include <algorithm>
#include <cmath>
#include <filesystem>
#include <fstream>

#include "error.h"

namespace periodic_signal_analyzer::model {

static const std::filesystem::path kProgramSourcePath = "./res/kernels/error_estimator_kernel.cl";

static const std::string kKernelName = "error_estimator_kernel";

struct __attribute__((packed)) KernelParameters {
    explicit KernelParameters(const Worker::Parameters& parameters)
        : phi{parameters.phi}, k{parameters.k}, n{parameters.n} {}

    cl_float phi;
    cl_uint k;
    cl_uint n;
};

struct __attribute__((packed)) KernelPointResult {
    auto Convert() -> Worker::PointResult {
        return Worker::PointResult{
            .m = m,
            .value = value,
            .mean_square_value_error = {mean_square_value_error[0], mean_square_value_error[1]},
            .amplitude_error = amplitude_error,
        };
    }

    cl_uint m;
    cl_float value;
    cl_float mean_square_value_error[2];
    cl_float amplitude_error;
};

OpenClWorker::OpenClWorker(const cl::Context& context) : context_{context} {
    std::ifstream ifstream{kProgramSourcePath, std::ios::in};
    if (ifstream.fail()) {
        throw Error{"Shader is not found in: " + kProgramSourcePath.string()};
    }

    std::string kernel_source{std::istreambuf_iterator<char>{ifstream},
                              std::istreambuf_iterator<char>{}};

    cl::Program::Sources program_sources{
        std::make_pair(kernel_source.c_str(), kernel_source.length())};
    auto program = BuildProgram(context, program_sources);

    cl_int error = CL_SUCCESS;
    kernel_ = cl::Kernel{program, kKernelName.c_str(), &error};
    if (error != CL_SUCCESS)
        throw Error{"Undefined reference to kernel: " + kKernelName, "OpenCL compilation error"};
}

auto OpenClWorker::DoWork(const Worker::Parameters& parameters) -> Result {
    cl_int error = CL_SUCCESS;
    cl::Event event;
    cl::CommandQueue queue{context_};

    size_t point_result_count = 2 * parameters.n - parameters.k + 1;
    cl::Buffer point_results_buffer(context_, CL_MEM_WRITE_ONLY,
                                    sizeof(KernelPointResult) * point_result_count, nullptr,
                                    &error);

    cl::Buffer parameters_buffer{context_, CL_MEM_READ_ONLY, sizeof(KernelParameters), nullptr,
                                 &error};

    // Pass parameters to Device
    KernelParameters kernel_parameters{parameters};
    error = queue.enqueueWriteBuffer(parameters_buffer, CL_TRUE, 0, sizeof(KernelParameters),
                                     &kernel_parameters, nullptr, &event);

    error = kernel_.setArg(0, point_results_buffer);
    error = kernel_.setArg(1, parameters_buffer);

    queue.finish();

    // Execute kernel on Device
    cl::NDRange local_size{16};
    size_t local_groups = std::ceil(float(point_result_count) / local_size[0]);
    cl::NDRange global_size{local_groups * local_size[0]};
    error = queue.enqueueNDRangeKernel(kernel_, cl::NullRange, global_size, local_size, nullptr,
                                       &event);
    queue.finish();
    if (error != CL_SUCCESS) {
        throw Error{"OpenCL Kernel runtime error"};
    }

    // Read results from Device
    std::vector<KernelPointResult> kernel_point_results(point_result_count);
    queue.enqueueReadBuffer(point_results_buffer, CL_TRUE, 0,
                            sizeof(KernelPointResult) * point_result_count,
                            kernel_point_results.data(), nullptr, &event);
    queue.finish();

    std::vector<PointResult> point_results(point_result_count);
    std::transform(std::begin(kernel_point_results), std::end(kernel_point_results),
                   std::begin(point_results),
                   std::bind(&KernelPointResult::Convert, std::placeholders::_1));
    return {.parameters = parameters, .point_results = std::move(point_results)};
}

auto OpenClWorker::BuildProgram(const cl::Context& context, cl::Program::Sources program_sources)
    -> cl::Program {
    cl::Program program{context, program_sources};
    try {
        program.build();
        return program;
    } catch (cl::Error& error) {
        std::ostringstream ss;
        ss << "Program build error: " << error.what() << std::endl;
        ss << "Program build log: " << std::endl;
        for (auto& device : context.getInfo<CL_CONTEXT_DEVICES>()) {
            cl_build_status status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device);
            if (status != CL_BUILD_ERROR) continue;

            ss << "\t" << device.getInfo<CL_DEVICE_NAME>() << ": "
               << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
        }

        throw Error{ss.str(), "OpenCL program build error"};
    }
}

}  // namespace periodic_signal_analyzer::model

#ifndef PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_MODEL_H
#define PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_MODEL_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "error.h"
#include "opencl.h"
#include "worker.h"

namespace periodic_signal_analyzer::model {

struct OpenClDeviceInfo {
    cl::Device device;
};

struct OpenClPlatformInfo {
    cl::Platform platform;
    std::vector<OpenClDeviceInfo> devices;
};

struct OpenClInfo {
    std::vector<OpenClPlatformInfo> platforms;
};

class PeriodicSignalAnalyzerModel {
  public:
    PeriodicSignalAnalyzerModel();

    void InitWorker(std::optional<cl::Device> device = std::nullopt);

    auto GetOpenClInfo() -> const OpenClInfo&;
    auto GetCurrentDevice() -> const std::optional<cl::Device>&;

    void SetCurrentError(std::optional<Error> error);
    auto GetCurrentError() -> const std::optional<Error>&;

    auto GetWorkParameters() -> const Worker::Parameters&;
    void SetWorkParameters(const Worker::Parameters& parameters);

    auto GetWorkResult() -> Worker::Result&;

    void RunWorker();

  private:
    static auto GatherOpenClInfo() -> OpenClInfo;

    std::optional<cl::Context> context_{};
    std::optional<cl::Device> device_{};
    std::optional<Error> error_{};

    OpenClInfo opencl_info_{};
    std::unique_ptr<Worker> worker_{};

    Worker::Parameters work_parameters_{};
    Worker::Result work_result_;
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_PERIODIC_SIGNAL_ANALYZER_MODEL_H

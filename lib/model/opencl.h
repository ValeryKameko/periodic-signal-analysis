#ifndef PERIODIC_SIGNAL_ANALYZER_OPENCL_H
#define PERIODIC_SIGNAL_ANALYZER_OPENCL_H

#define __CL_ENABLE_EXCEPTIONS

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif

#endif  // PERIODIC_SIGNAL_ANALYZER_OPENCL_H

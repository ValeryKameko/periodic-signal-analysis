#ifndef PERIODIC_SIGNAL_ANALYZER_CPU_WORKER_H
#define PERIODIC_SIGNAL_ANALYZER_CPU_WORKER_H

#include "worker.h"

namespace periodic_signal_analyzer::model {

class CpuWorker : public Worker {
    auto DoWork(const Parameters &parameters) -> Result override;
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_CPU_WORKER_H

project(periodic-signal-analyzer-model)

add_library(${PROJECT_NAME}
        error.h error.cc
        worker.h
        cpu_worker.h cpu_worker.cc
        opencl_worker.h opencl_worker.cc
        periodic_signal_analyzer_model.h periodic_signal_analyzer_model.cc)

add_library(periodic-signal-analyzer::model
        ALIAS ${PROJECT_NAME})

target_link_libraries(${PROJECT_NAME}
        PRIVATE ${OpenCL_LIBRARY} stdc++fs)

target_include_directories(${PROJECT_NAME}
        PUBLIC . ${OpenCL_INCLUDE_DIR})

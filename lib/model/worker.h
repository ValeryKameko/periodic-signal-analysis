#ifndef PERIODIC_SIGNAL_ANALYZER_WORKER_H
#define PERIODIC_SIGNAL_ANALYZER_WORKER_H

#include <vector>

namespace periodic_signal_analyzer::model {

class Worker {
  public:
    struct Parameters {
        unsigned int n;
        unsigned int k;
        float phi;

        auto operator<=>(const Parameters&) const = default;
    };

    struct PointResult {
        unsigned int m;
        float value;
        float mean_square_value_error[2];
        float amplitude_error;
    };

    struct Result {
        Parameters parameters;
        std::vector<PointResult> point_results;
    };

    Worker() = default;
    virtual ~Worker() = default;

    virtual auto DoWork(const Parameters& parameters) -> Result = 0;
};

}  // namespace periodic_signal_analyzer::model

#endif  // PERIODIC_SIGNAL_ANALYZER_WORKER_H
